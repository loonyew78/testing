package com.example.controller;

import com.example.model.Person;
import com.example.repository.PersonRepository;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class PersonController {
    @Autowired
    private PersonRepository personRepository;
    private String applicationName = "Person App";
    private final String ENTITY_NAME = "Person";
    @GetMapping("/person")
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }
    @GetMapping("/person/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable UUID id) {
        Optional<Person> person = personRepository.findOneById(id);
        return ResponseUtil.wrapOrNotFound(person);
    }
    @PostMapping("/person")
    public ResponseEntity<Void> createPerson(@Valid @RequestBody Person person) throws URISyntaxException {
        Person result = personRepository.save(person);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, String.valueOf(result.getId()))).build();
    }
    @DeleteMapping("/person/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable UUID id) throws Exception {
        Optional<Person> existingPerson = personRepository.findOneById(id);
        if( !existingPerson.isPresent()) {
            throw new Exception("Deleting Non-Existing User");
        } else {
           personRepository.delete(existingPerson.get());
        }
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @PutMapping("/person/{id}")
    public ResponseEntity<Person> updatePerson(@RequestBody Person person, @PathVariable UUID id) throws Exception {
        Optional<Person> existingPerson = personRepository.findOneById(id);
        if( !existingPerson.isPresent()) {
            throw new Exception("Updating Non-Existing User");
        }
        Person result = personRepository.save(person);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString())).build();
    }
}
